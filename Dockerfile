FROM python:3.9.1

WORKDIR /app

COPY requirements.txt /app
RUN pip install -r requirements.txt
COPY . /app

EXPOSE 8881 

CMD [ "python", "/app/zoomer.py" ]

