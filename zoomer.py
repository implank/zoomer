from flask import Flask, request, redirect

app = Flask(__name__)

DefaultPort = '8881'
DefaultLoc = f'http://127.0.0.1:{DefaultPort}'
NetLoc = DefaultLoc

@app.route('/ping')
def ping():
	return 'pong', 200


@app.route('/set')
def setPath():
	base = request.args.get('base')
	global NetLoc
	NetLoc = base
	return f'New server at {NetLoc}', 200


@app.route('/api/<path:path>')
def api(path:str):
	if NetLoc is None:
		return 'No server exist', 404
	print(f'{NetLoc}/api/{path}')
	return redirect(f'{NetLoc}/api/{path}')


@app.route('/redirect/<path:path>')
def redirectPath(path:str):
	return redirect(f'{NetLoc}/{path}')


@app.route('/show')
def show():
	return f'NetLoc is {NetLoc}', 200


if __name__ == '__main__':
	app.run(host='0.0.0.0', port=DefaultPort, debug=True)
